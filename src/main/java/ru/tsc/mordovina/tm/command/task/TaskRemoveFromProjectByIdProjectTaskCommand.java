package ru.tsc.mordovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class TaskRemoveFromProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-remove-from-project-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Removes task from project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
    }

}
