package ru.tsc.mordovina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractAuthCommand;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class LoginCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login user to system";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
