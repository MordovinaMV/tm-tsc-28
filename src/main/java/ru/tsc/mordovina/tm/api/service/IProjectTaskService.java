package ru.tsc.mordovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @Nullable
    List<Task> findTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    void bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void removeById(@NotNull String userId, @Nullable String projectId);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @Nullable String name);

}
